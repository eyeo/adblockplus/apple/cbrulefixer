# CBRuleRepair: DEPRECATED!!

## Overview
A JavaScript command-line tool that fixes misconverted rules in ABP2Blocklist's `easylist -> json` conversion.

## Detail
CBRuleRepair is a quick script made to both identify and fix rules that have been incorrectly converted with our main conversion tool [ABP2Blocklist](https://gitlab.com/eyeo/adblockplus/abp2blocklist).

CBRuleRepair does the following:
* Identifies rules with actions `ignore-previous-rules` & `selector`.
* If selector is generic, removes selector from generic rule array and creates new rule from misconverted rule.
* If selector is not generic, adds misconverted rule URL as an exception where selectors match.
* Removes the broken rules.

## Usage
`node cbrulerepair.js <Input Filename> <Output Filename>`

## Swift Version
The Swift version is now considered deprecated and can be found in the `Swift-Deprecated` directory, along with it's own README file.
