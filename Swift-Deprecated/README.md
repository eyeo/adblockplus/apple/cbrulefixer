# CBRuleRepair

## Overview
A Swift command-line tool that fixes misconverted rules in ABP2Blocklist's `easylist -> json` conversion.

## Detail
CBRuleRepair is a quick script made to both identify and fix rules that have been incorrectly converted with our main conversion tool [ABP2Blocklist](https://gitlab.com/eyeo/adblockplus/abp2blocklist).

CBRuleRepair does the following:
* Identifies rules with actions `ignore-previous-rules` & `selector`.
* If selector is generic, removes selector from generic rule array and creates new rule from misconverted rule.
* If selector is not generic, adds misconverted rule URL as an exception where selectors match.
* Removes the broken rules.

## Usage
`CBRuleRepair -i path/to/input.json -o /path/to/output.json`

## Options
`--input`, `-i`    Path to filterlist that requires fixing.
` --output`, `-o`   Path to output fixed filterlist.
`--help`        Display available options

## Build

To build a binary from source run the following:

`swift build --configuration release`

This will output to `.build/release/CBRuleRepair`

## Installation

After running the build stage, run the following: 

`cp -f .build/release/CBRuleRepair /usr/local/bin/CBRuleRepair`

Now you can run CBRuleRepair from the terminal at any time. 