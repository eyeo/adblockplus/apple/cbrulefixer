/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

typealias FilterList = [FilterListElement]

class FilterListElement: Codable {
    internal init(trigger: Trigger, action: Action, manualFix: ManualFix?) {
        self.trigger = trigger
        self.action = action
        self.manualFix = manualFix
    }
    var trigger: Trigger
    var action: Action
    var manualFix: ManualFix?

    enum CodingKeys: String, CodingKey {
        case trigger, action
        case manualFix = "manual-fix"
    }
}

struct Action: Codable {
    var type: TypeEnum
    var selector: String?
}

enum TypeEnum: String, Codable {
    case block = "block"
    case cssDisplayNone = "css-display-none"
    case ignorePreviousRules = "ignore-previous-rules"
}

struct ManualFix: Codable {
    var delete: Bool
}

struct Trigger: Codable {
    var urlFilter: String
    let urlFilterIsCaseSensitive: Bool?
    let ifDomain: [String]?
    let ifTopUrl: [String]?
    let resourceType: [ResourceType]?
    var unlessDomain: [String]?
    var unlessTopUrl: [String]?
    let loadType: [LoadType]?

    enum CodingKeys: String, CodingKey {
        case urlFilter = "url-filter"
        case urlFilterIsCaseSensitive = "url-filter-is-case-sensitive"
        case ifDomain = "if-domain"
        case ifTopUrl = "if-top-url"
        case resourceType = "resource-type"
        case unlessDomain = "unless-domain"
        case unlessTopUrl = "unless-top-url"
        case loadType = "load-type"
    }
}

enum LoadType: String, Codable {
    case firstParty = "first-party"
    case thirdParty = "third-party"
}

enum ResourceType: String, Codable {
    case document = "document"
    case font = "font"
    case image = "image"
    case media = "media"
    case popup = "popup"
    case raw = "raw"
    case script = "script"
    case styleSheet = "style-sheet"
}

struct RuleData {
    var selector: String
    var url: [String]
    func urlMatches(urlArray: [String]) -> Bool {
        self.url == urlArray
    }
    func selectorMatches(selector: String) -> Bool {
        self.selector == selector
    }
}
