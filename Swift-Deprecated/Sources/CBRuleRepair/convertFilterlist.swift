/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

func convertFilterlist(inputPath: String, outputPath: String) {

    let filename = (inputPath as NSString).lastPathComponent
    print("\n📝 - Converting \(filename)")

    // Get JSON from main bundle
    let inputFileURL = URL(fileURLWithPath: inputPath)
    let jsonData = try! Data(contentsOf: inputFileURL)

    // Decode JSON data
    var listDecoded = try! JSONDecoder().decode(FilterList.self, from: jsonData)

    // Constants
    let genericURLFilter = "^https?://"
    var count = 0
    var firstRuleSelectors = listDecoded[0].action.selector?.components(separatedBy: ",") ?? [""]
    let firstRuleExceptions = listDecoded[0].trigger.unlessTopUrl
    var newRules = FilterList()
    var ruleDataArray = [RuleData]()
    var uniqueSelectors = [String]()

    // Loop over rules within filterlist that have both `ignore-previous-rules` and `selector` actions.
    for rule in listDecoded where rule.action.type == .ignorePreviousRules && rule.action.selector != nil {

        // Increment count
        count+=1

        // Get an array of selectors in the rule
        let ruleSelectorArray = rule.action.selector?.components(separatedBy: ",") ?? [""]

        // Iterate over selectors within the rule
        ruleSelectorArray.forEach { (selector) in
            // If selector is generic, then mark to be removed from generic selector array and
            // create a new rule to be added.
            // Else: If selector is not generic, then add an exception to the rule where selectors match.
            if firstRuleSelectors.contains(selector) {
                let newRuleData = RuleData(selector: selector, url: [rule.trigger.urlFilter])
                ruleDataArray.append(newRuleData)
                if !uniqueSelectors.contains(selector) {uniqueSelectors.append(selector)}
            } else {
                for exceptionrule in listDecoded where exceptionrule.action.type == .cssDisplayNone
                    && exceptionrule.action.selector == selector
                    && exceptionrule.trigger.urlFilter != rule.trigger.urlFilter
                    && !(exceptionrule.trigger.unlessDomain?.contains(rule.trigger.urlFilter) ?? false) {
                        let exceptionRuleSelectorArray = exceptionrule.action.selector?.components(separatedBy: ",") ?? [""]
                        if exceptionRuleSelectorArray.contains(selector) {
                            if (exceptionrule.trigger.unlessTopUrl != nil) {
                                exceptionrule.trigger.unlessTopUrl?.append(rule.trigger.urlFilter)
                            } else {
                                exceptionrule.trigger.unlessTopUrl = [rule.trigger.urlFilter]
                            }
                        }
                }
            }
            // Mark the processed rule for deletion
            let deletion = ManualFix.init(delete: true)
            rule.manualFix = deletion
        }
    }

    // Remove processed rules
    print("List Decoded Count Before: \(listDecoded.count)")
    listDecoded = listDecoded.filter {$0.manualFix?.delete != true}
    print("List Decoded Count After: \(listDecoded.count)")

    print("Rule Data Count: \(ruleDataArray.count)")

    // Combine URLS that target the same selectors to further reduce rule count
    var ruleDataArrayCondensed = [RuleData]()
    for selector in uniqueSelectors {
        var tempRule = RuleData(selector: selector, url: [])
        for rule in ruleDataArray where rule.selector == selector {
            tempRule.url.insert(contentsOf: rule.url, at: tempRule.url.endIndex)
        }
        ruleDataArrayCondensed.append(tempRule)
        // Remove selectors from first generic hide rule as they are now non-generic
        firstRuleSelectors = firstRuleSelectors.filter {$0 != selector}
    }

    // Create new rules out of the condensed rules
    for rule in ruleDataArrayCondensed {
        var unlessTopURL = rule.url
        unlessTopURL.append(contentsOf: firstRuleExceptions ?? [""])
        newRules.append(FilterListElement(trigger: .init(urlFilter: genericURLFilter,
                                                         urlFilterIsCaseSensitive: true,
                                                         ifDomain: nil,
                                                         ifTopUrl: nil,
                                                         resourceType: nil,
                                                         unlessDomain: nil,
                                                         unlessTopUrl: unlessTopURL,
                                                         loadType: nil),
                                          action: .init(type: .cssDisplayNone,
                                                        selector: rule.selector),
                                          manualFix: nil)
        )
    }

    print("New Rules Count: \(newRules.count)")

    // Replace the selectors in the first rule (generic selectors) with a mutated array
    // that removed the now non-generic selectors.
    let newFirstRuleSelectors = firstRuleSelectors.joined(separator: ",")
    listDecoded[0].action.selector = newFirstRuleSelectors

    // Add new rules into array under the first generic rule
    for rule in newRules {
        listDecoded.insert(rule, at: 1)
    }

    // Re-encode JSON
    let encoder = JSONEncoder()
    encoder.outputFormatting = [.prettyPrinted, .withoutEscapingSlashes]
    let listEncoded = try! encoder.encode(listDecoded)

    // Get URL to the the documents directory in the sandbox
    let outputFileURL = URL(fileURLWithPath: outputPath)
    try! listEncoded.write(to: outputFileURL)

    let outputPathWithoutFilename = (outputPath as NSString).deletingLastPathComponent

    print("🥳 - Converted \(count) broken rules in \(filename)")
    print("🗂  - \(filename) saved to \(outputPathWithoutFilename)\n")
}
