/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation
import SPMUtility

let arguments = ProcessInfo.processInfo.arguments.dropFirst()

let parser = ArgumentParser(usage: "<options>", overview: "A Swift command-line tool that fixes misconverted rules in ABP2Blocklist's `easylist -> json` conversion.")
let inputArgument = parser.add(option: "--input", shortName: "-i", kind: String.self, usage: "Path to filterlist that requires fixing.")
let outputArgument = parser.add(option: "--output", shortName: "-o", kind: String.self, usage: "Path to output fixed filterlist.")

do {
    let parsedArguments = try parser.parse(Array(arguments))
    let input = parsedArguments.get(inputArgument)!
    let output = parsedArguments.get(outputArgument)!
    convertFilterlist(inputPath: input, outputPath: output)
} catch {
    print("Banner creation failed with \(error)")
}

print("""
---------------------
✨ Fixing Finished ✨
---------------------
""")
