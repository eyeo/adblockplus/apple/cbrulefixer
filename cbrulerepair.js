/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */


import {readFileSync, writeFile} from "fs";

class Rule
{
  constructor(selector, url)
  {
    // `this` is the instance which is currently being created
    this.selector = selector;
    this.url = url;
  }
}

// Get file input. If no input, throw error & exit.
if (process.argv.length < 4)
{
  console.error("Incorrect usage!");
  console.error("Usage: node " +
                process.argv[1] +
                " <Input Filename> <Output Filename>");
  process.exit(1);
}

// Declare containers.
let genericURLFilter = "^https?://";
let uniqueSelectors = new Set();
let ruleDataArray = [];
let newRules = [];
let count = 0;
let filename = process.argv[2];
let output = process.argv[3];

// Read contents of file.
let contents = readFileSync(filename, {encoding: "utf-8"});
try
{
  // Parse the JSON file to allow for manipulation.
  let data = JSON.parse(contents);

  // Containers for first rule exceptions & selectors, to be used later.
  let firstRuleExceptions = data[0].trigger["unless-top-url"];
  let firstRuleSelectors = data[0].action.selector.split(",");

  // Iterate through the records, starting at index 1.
  for (let record of data)
  {
    // Step in to rules that have both `ignore-previous-rules`
    // and `selector` actions.
    if (record.action.type === "ignore-previous-rules" &&
        record.action.selector)
    {
      // Increment count.
      count++;

      // Get selectors in rule.
      let ruleSelectorArray = record.action.selector.split(",");

      // Iterate over selectors within rule.
      for (let selector of ruleSelectorArray)
      {
        // If selector is generic, then mark to be removed from generic
        // selector array and create a new rule to be added.
        // Else: If selector is not generic, then add an exception to
        // the rule where selectors match.
        if (firstRuleSelectors.includes(selector))
        {
          let newRuleData = new Rule(selector,
                                     record.trigger["url-filter"]);
          ruleDataArray.push(newRuleData);
          if (!uniqueSelectors.has(selector))
          {
            uniqueSelectors.add(selector);
          }
        }
        else
        {
          for (let exceptionRecord of data)
          {
            if (exceptionRecord.action.type == "css-display-none" &&
                exceptionRecord.action.selector == selector &&
                exceptionRecord.trigger["url-filter"] !=
                  record.trigger["url-filter"])
            {
              if (!exceptionRecord.trigger["unless-domain"] ||
                exceptionRecord.trigger["unless-domain"] &&
                !exceptionRecord.trigger["unless-domain"].
                  includes(record.trigger["url-filter"]))
              {
                let exceptionRuleSelectorArray =
                    exceptionRecord.action.selector.split(",");
                if (exceptionRuleSelectorArray.includes(selector))
                {
                  if (!exceptionRecord.trigger["unless-top-url"])
                  {
                    exceptionRecord.trigger["unless-top-url"] = [];
                  }
                  exceptionRecord.trigger["unless-top-url"].push(
                    record.trigger["url-filter"]);
                }
              }
            }
          }
        }
        // Mark the processed rule for deletion.
        record.delete = true;
      }
    }
  }

  for (let i = data.length - 1; i >= 0; i--)
  {
    let record = data[i];
    if (record.delete === true)
    {
      data.splice(i, 1);
    }
  }

  // Combine URLs that target the same selectors to further reduce rule count.
  let ruleDataArrayCondensed = [];
  for (let selector of uniqueSelectors)
  {
    let tempRule = new Rule(selector, []);
    for (let rule of ruleDataArray)
    {
      if (rule.selector === selector)
      {
        tempRule.url.push(rule.url);
      }
    }
    ruleDataArrayCondensed.push(tempRule);

    // Remove selectors from first generic hide rule as
    // they are now non-generic.
    let selectors = firstRuleSelectors.filter(x => x !== selector);
    firstRuleSelectors = selectors;
  }

  // Create new rules out of the condensed rules
  for (let rule of ruleDataArrayCondensed)
  {
    let unlessTopURL = rule.url;
    unlessTopURL.push(...firstRuleExceptions);
    newRules.push({
      trigger: {
        "url-filter": genericURLFilter,
        "url-filter-is-case-sensitive": true,
        "unless-top-url": unlessTopURL
      },
      action: {
        type: "css-display-none",
        selector: rule.selector
      }
    });
  }

  // Replace the selectors in the first rule (generic selectors)
  // with a mutated array that removed the now non-generic selectors.
  let newFirstRuleSelectors = firstRuleSelectors.join();
  data[0].action.selector = newFirstRuleSelectors;

  // Add new rules into array under the first generic rule.
  for (let rule of newRules)
  {
    data.splice(1, 0, rule);
  }

  // Re-encode JSON & Write to file.
  let jsonString = JSON.stringify(data, null, 2);
  writeFile(output, jsonString, err =>
  {
    if (err)
    {
      console.log("Error writing file", err);
    }
    else
    {
      console.log("Successfully wrote file");
    }

    console.error("Converted " + count + " broken rules in " + filename);
  });
}
catch (err)
{
  console.error(err);
}
